<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
    	'firstName',
    	'lastName',
    	'companyID',
    	'email',
    	'phone'
    	];

     public function getfirstNameAttribute($value)
    {
        return title_case($value);
    }

    public function getlastNameAttribute($value)
    {
        return title_case($value);
    }

    public function getemailAttribute($value)
    {
    	if($value == null)
        {
        	return  'Not Available';
        }
    	else return $value;
    }

    public function getphoneAttribute($value)
    {
    	if($value == null)
        {
        	return  'Not Available';
        }
    	else return $value;
    }
}
