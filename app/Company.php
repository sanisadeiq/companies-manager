<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
    	'name',
    	'email',
    	'logo',
    	'website'
    ];

     public function getnameAttribute($value)
    {   
     return  title_case($value);
    }

    public function getemailAttribute($value)
    {
    	if($value == null)
        {
        	return  'Not Available';
        }
    	else return $value;
    }

    public function getlogoAttribute($value)
    {
    	if($value == null)
        {
        	return  'Not Available';
        }
    	else return $value;
    }

    public function getwebsiteAttribute($value)
    {
    	if($value == null)
        {
        	return  'Not Available';
        }
    	else return $value;
    }

    public static function logoupload($image)
    {
          $name = str_random(5);
          $destinationPath = public_path(); 
          $extension = $image->getClientOriginalExtension(); 
          $fileName = $name.'.'.$extension; 
          $image->move($destinationPath, $fileName); 

          return $fileName;
    }

    public function employees()
    {
        return $this->hasMany('App\Employee', 'companyID');
    }
}
