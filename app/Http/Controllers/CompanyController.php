<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;

use App\Http\Requests\CompanyRequest;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        if($request->hasFile('logo'))
        {
          $fileName = Company::logoupload($request['logo']);
        }

        $company = new Company($request->all());
        $company->logo = isset($fileName) ? $fileName : ' ';
        $company->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        return view('editCompanyInformation', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);

        if($request->hasFile('logo'))
        {
          \File::delete($company->logo);

          $fileName = Company::logoupload($request['logo']);
        }

        $company->name = $request['name'];
        $company->website = $request['website'];
        $company->logo = isset($fileName) ? $fileName : ' ';
        $company->email = $request['email'];
        $company->save();

        return redirect()->route('home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);

        if($company->logo != null);
        {
           \File::delete($company->logo); 
        }
        
        $company->delete();

        return back();
    }
}
