<?php

namespace App\Http\Controllers;

use App\Employee;

use App\Company;

use App\Http\Requests\EmployeeRequest;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $company = Company::find($id);

        //laravel does not support pagination on eager loading
        $employees = Employee::where('companyID', $id)->paginate(10);

        return view('companyDashboard', compact('company', 'employees', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
       // dd($request->all());
        $employee = New Employee($request->all());
        $employee->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);

        return view('editEmployeeInformation', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        Employee::where('id', $id)
                ->update([
                    'firstName' =>  $request['firstName'],
                    'lastName'  =>  $request['lastName'],
                    'phone'     =>  $request['phone'],
                    'email'     =>  $request['email'],
                ]);
        $employee = Employee::find($id);
        return redirect()->route('companydashboard', ['id'  =>  $employee->companyID]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();

        return back();
    }
}
