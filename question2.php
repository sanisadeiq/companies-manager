<?php
function bracechecker($s) {

    static $open = 0;

    if ( (substr($s, 0, 1) == ')') || (substr($s, -1, 1) == '(') ) {
        return false;
    }
    for ( $i = 0; $i < count($s); $i++ ) {
        if ( substr($s, $i, 1) == ')' ) {
            $open++; //increment
        }
        else {
            if ( $open < 0 ) {
                return false;
            }
            $open--; //decrement
        }
    }
    return true;
}
$tests = array(
    '(())' => '', /* pass */
    ')()()' => '', /*  fail  */
    '()()(' => '', /* fail */
    '()()())()()' => '' /*  fail */
);
foreach ( $tests as $k => $v ) {
    $tests[$k] = ( bracechecker($k) ? 'YES' : 'NO' );
}

?>