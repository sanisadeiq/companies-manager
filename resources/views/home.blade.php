@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="col-md-6 offset-md-3">
                        <h3 align="center">Add Company</h3>
                        <form method="POST" action="{{ url('/add-company') }}" enctype="multipart/form-data">
                            @csrf
                                @include('partials.forms.company')
                        </form>
                    </div>

                    <div class="col-md-12" style="margin-top: 30px">
                        <h3 align="center">Companies</h3>

                        @if($companies->isEmpty())
                        <h4 align="center" style="margin-top: 20px">No Companies added</h4>
                        @else
                        @include('partials.tables.company')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
