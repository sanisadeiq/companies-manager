@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{$company->name}}</div>

                <div class="card-body">
                    <div class="col-md-6 offset-md-3">
                        <h3 align="center">Edit Information</h3>
                        <form method="POST" action="{{ url('/update-company-information', [$company->id]) }}" enctype="multipart/form-data">
                            @csrf
                                @include('partials.forms.company')
                        </form>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
