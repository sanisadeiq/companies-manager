@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{$company->name}}</div>

                <div class="card-body">
                    <div class="col-md-6 offset-md-3">
                        <h3 align="center">Add Employee</h3>
                        <form method="POST" action="{{ url('/add-employee') }}" enctype="multipart/form-data">
                            @csrf
                                @include('partials.forms.employee')
                        </form>
                    </div>

                    <div class="col-md-12" style="margin-top: 30px">

                        @if($company->employees->isEmpty())
                        <h4 align="center" style="margin-top: 20px">No employees added</h4>
                        @else
                        @include('partials.tables.employee')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
