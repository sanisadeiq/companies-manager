 <div class="form-group">
                        <label for="name">Company Name</label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" value="{{ isset($company->name) ? $company->name : old('name') }}" placeholder="Name">
                        @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                      </div>

                       <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ isset($company->email) ? $company->email : old('email') }}" placeholder="email">
                        @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                      </div>

                      <div class="form-group">
                        <label for="logo">Logo</label>
                        <input type="file" class="form-control{{ $errors->has('logo') ? ' is-invalid' : '' }}" id="logo" name="logo" value="{{ isset($company->logo) ? $company->logo : old('logo') }}">
                        @if ($errors->has('logo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                        @endif
                      </div>

                      <div class="form-group">
                        <label for="website">Website</label>
                        <input type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" id="website" name="website" value="{{ isset($company->website) ? $company->website : old('website') }}" placeholder="website">
                        @if ($errors->has('website'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                      </div>

                      <div class="form-group">
                          <button class="btn btn-primary">Add</button>
                      </div>