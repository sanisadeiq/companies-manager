  <div class="row">
    <div class="col">
      <input type="text" class="form-control{{ $errors->has('firstName') ? ' is-invalid' : '' }}" name="firstName" value="{{ isset($employee->firstName) ? $employee->firstName : old('firstName') }}" placeholder="First name">
       @if ($errors->has('firstName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                        @endif
    </div>
    <div class="col">
      <input type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ isset($employee->lastName) ? $employee->lastName : old('lastName') }}" placeholder="Last name">
        @if ($errors->has('lastName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                        @endif
    </div>
  </div>
  <div class="row" style="margin-top: 20px">
    <div class="col">
      <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ isset($employee->email) ? $employee->email : old('email') }}" placeholder="Email">
        @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
    </div>
    <div class="col">
      <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ isset($employee->phone) ? $employee->phone : old('phone') }}" placeholder="Phone Number">
        @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif
      <input type="hidden" name="companyID" value="{{isset($id) ? $id : $employee->companyID}}">
    </div>
  </div>
   <div class="row" style="margin-top: 20px">
    <div class="col">
      <button class="btn btn-primary">Add</button>
    </div>
  </div>