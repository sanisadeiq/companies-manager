<span class="float-right">{{'Number of Companies:'.' '.$companies->total()}}</span>
<table class="table table-hover">
                            <thead>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Website
                                </th>
                            </thead>
                            <tbody>
                                @foreach($companies as $company)
                                <tr>
                                    <td>
                                        {{$loop->index + 1}}
                                    </td>
                                    <td>
                                        
                                            @if($company->logo != ' ')
                                            <img src="{{ asset($company->logo) }}" class="rounded float-left" width="150px" style="margin-bottom: 20px">
                                           @endif
                                            &nbsp <a href="{{ url('/company-dashboard', [$company->id]) }}"> {{$company->name}}
                                            </a>
                                    </td>
                                    <td>
                                        {{$company->email}}
                                    </td>
                                    <td>
                                        {{$company->website}}
                                    </td>
                                    <td>
                                        <a href="{{ url('/edit-company-information', [$company->id]) }}">
                                        Edit
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('/delete-company', [$company->id]) }}">
                                        Delete
                                    </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <span class="float-right">{{ $companies->links() }}</span>