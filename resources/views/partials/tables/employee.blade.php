<span class="float-right">{{'Number of Employees:'.' '.$employees->total()}}</span>
<table class="table table-hover">
                            <thead>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Phone
                                </th>
                            </thead>
                            <tbody>
                                @foreach($employees as $employee)
                                <tr>
                                    <td>
                                        {{$loop->index + 1}}
                                    </td>
                                    <td>
                                        
                                         {{$employee->firstName.' '.$employee->lastName}}
                                          
                                    </td>
                                    <td>
                                        {{$employee->email}}
                                    </td>
                                    <td>
                                        {{$employee->phone}}
                                    </td>
                                    <td>
                                        <a href="{{ url('/edit-employee-information', [$employee->id]) }}">
                                        Edit
                                    </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('/delete-employee', [$employee->id]) }}">
                                        Delete
                                    </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <span class="float-right">{{ $employees->links() }}</span>