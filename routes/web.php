<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

// Password reset routes...
Route::get('password/reset', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/add-company', 'CompanyController@store');
Route::get('/edit-company-information/{id}', 'CompanyController@edit');
Route::post('/update-company-information/{id}', 'CompanyController@update');
Route::get('/delete-company/{id}', 'CompanyController@destroy');

Route::get('/company-dashboard/{id}', 'EmployeeController@index')->name('companydashboard');
Route::post('/add-employee', 'EmployeeController@store');
Route::get('/edit-employee-information/{id}', 'EmployeeController@show');
Route::post('/update-employee/{id}', 'EmployeeController@update');
Route::get('/delete-employee/{id}', 'EmployeeController@destroy');
